package co.simplon.promo16.springwithjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringwithjdbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringwithjdbcApplication.class, args);

		
	}

}
