package co.simplon.promo16.springwithjdbc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import co.simplon.promo16.springwithjdbc.dao.DaoDog;
import co.simplon.promo16.springwithjdbc.entity.Dog;

@Controller
public class DogController {
    @Autowired
    private DaoDog dao;

    @GetMapping("/")
    public String index(Model model) {
        // dao.findAll();
        model.addAttribute("dogs", dao.findAll());
        model.addAttribute("newDog", new Dog());
        return "index";
    }

    @PostMapping("/add")
    public String addDog(@ModelAttribute("newDog") Dog newDog, Model model) {
        dao.add(newDog);

        return "redirect:/";
    }
}
