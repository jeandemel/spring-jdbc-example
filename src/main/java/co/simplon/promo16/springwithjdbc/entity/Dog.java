package co.simplon.promo16.springwithjdbc.entity;

import java.sql.Date;

public class Dog {
    private int id;
    private String name;
    private String breed;
    private Date birthdate;

    public Dog() {
    }
    public Dog(String name, String breed, Date birthdate) {
        this.name = name;
        this.breed = breed;
        this.birthdate = birthdate;
    }
    public Dog(int id, String name, String breed, Date birthdate) {
        this.id = id;
        this.name = name;
        this.breed = breed;
        this.birthdate = birthdate;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getBreed() {
        return breed;
    }
    public void setBreed(String breed) {
        this.breed = breed;
    }
    public Date getBirthdate() {
        return birthdate;
    }
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
}
