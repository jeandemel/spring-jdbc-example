package co.simplon.promo16.springwithjdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.springwithjdbc.entity.Dog;

@Repository
public class DaoDog {
    @Autowired
    private DataSource datasource;

    public List<Dog> findAll() {
        Connection cnx = null;
        List<Dog> list = new ArrayList<>();
        try {
            cnx = datasource.getConnection();

            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM dog");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {

                list.add(new Dog(rs.getInt("id"), rs.getString("name"), rs.getString("breed"),
                        rs.getDate("birth_date")));
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return list;
    }

    public int add(Dog dog) {
        Connection cnx = null;
        
        try {
            cnx = datasource.getConnection();

            PreparedStatement stmt = cnx.prepareStatement("INSERT INTO dog (name,breed,birth_date) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, dog.getName());
            stmt.setString(2, dog.getBreed());
            stmt.setDate(3, dog.getBirthdate());
            stmt.executeUpdate();
            
            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next()) {
                dog.setId(rs.getInt(1));

            }
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, datasource);
        }
        return dog.getId();
    }

}
