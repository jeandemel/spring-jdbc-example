package co.simplon.promo16.springwithjdbc;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;

import co.simplon.promo16.springwithjdbc.dao.DaoDog;
import co.simplon.promo16.springwithjdbc.entity.Dog;

@ActiveProfiles("test")
@SpringBootTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
class SpringwithjdbcApplicationTests {
	@Autowired
	private DaoDog dao;

	
	@Test
	void testFindAll() {
		assertEquals(2, dao.findAll().size());
	}
	@Test
	void testAdd() {
		Dog dog = new Dog("from_test", "breed_test", new Date(1642975965));
		dao.add(dog);
		assertEquals(dog.getId(), 3);
	}

	

}
